const n = 5;
let contador = 0;
while(contador < n){
    console.log(typeof(contador));
    console.log(contador)
    contador++; // Incrementa mais 1 a variável antes do ++
}
for(let contador = 0; contador <n; contador++) {
    console.log(contador);
}

// Procurar depois isso para entender, switch dos loops.
// do{

// }while(contador < n);

let jogadores = ["Curry", "Durant", "Doncic","Young"]
for (jogador in jogadores){  //in pega os índices!
    console.log(typeof(jogador));
}

for (jogador of jogadores){ //of pega os itens!
    console.log(jogador);
}
